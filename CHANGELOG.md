## 3.5.4 - Under the spotlight - 2025-01-03

### Added
- 🧑‍🎤 Added the episode author on the list and modal
- ✨ Added cover image and episode infos on desktop and mobile notifications

## 3.5.3 - Hangover - 2025-01-03

### Fixed
- 🚑 Regression on filters

## 3.5.2 - Happy New Year - 2025-01-01

### Changed
- 🌐 Updated german language (thanks markus phi)

🎉 Happy new year !

## 3.5.1 - Merry Christmas - 2024-12-24

### Fixed
- 🐛 Filters reseted on page reload, now working.
[#231](https://git.crystalyx.net/Xefir/repod/issues/231) reported by @nolan

🎁 Merry Christmas everyone !

## 3.5.0 - Select all peaceful moments, unselect sadness - 2024-12-11

### Added
- ✨ Select severall episodes and mark them as read/unread

> Click on an episode cover to select it 😉

- 🌐 New platform to manage and help to translate RePod

> You can contribute to translate the app in your language and you don't need to have any development background to do so !
> Please join the effort at our **[Weblate](https://translate.crystalyx.net/projects/repod/gitea/)** project.
> Thank you so much if you decide to participate ❤️

## 3.4.1 - Skip & Chill - 2024-11-12

### Changed
- 💄 Make a little gap between player's controls

### Fixed
- 📝 Descriptions are now well formatted
- ⏩ Chapters supported !
> Click on a timestamp in descriptions to skip to the specified part of the podcast

## 3.4.0 - Good Night - 2024-11-09

### Added
- ♿ Improve accessibility by adding titles when missing
- ✨ Playback speed and volume setting doesn't stick
[#185](https://git.crystalyx.net/Xefir/repod/issues/185) reported by @SteveDinn
- ✨ Skip back or forward
[#159](https://git.crystalyx.net/Xefir/repod/issues/159) reported by @moonlike8812
- ✨ Sleep timer
[#119](https://git.crystalyx.net/Xefir/repod/issues/119) reported by @Markusphi and @OiledAmoeba

### Changed
- 💄 Add padding around favorites on mobile

### Fixed
- 🔒 Increase security when Nextcloud is in debug mode

## 3.3.2 - What a Nightmare - 2024-10-24

### Fixed
- 🚑 Revert [#178](https://git.crystalyx.net/Xefir/repod/issues/178) not working on big subscriptions lists
[#182](https://git.crystalyx.net/Xefir/repod/issues/182) reported by @SteveDinn

## 3.3.1 - Breaking the Loop - 2024-10-24

### Changed
- ⚡ Speed up the loading time of subscriptions
[#178](https://git.crystalyx.net/Xefir/repod/issues/178) reported by @MikeAndrews

### Fixed
- 🐛 Prevent Firefox for going nuts when having Plasma Integration addon installed
[#164](https://git.crystalyx.net/Xefir/repod/issues/164) reported by @cichy1173, @Share1440 and @mark.collins

## 3.3.0 - Into The Jet Lag - 2024-10-18

### Changed
- 🧑‍💻 CSS isn't mixed in the main JS file anymore

### Fixed
- 🐛 App won't load on Firefox 115
[#158](https://git.crystalyx.net/Xefir/repod/issues/158) reported by @Jaunty and @mark.collins
- 🔇 Volume slider didn't work properly

### Deprecated
- 💣 Require Nextcloud 29 or more

## 3.2.0 - Typing fast - 2024-09-15

### Added
- 📝 Add Cardo to list of compatible clients
[#176](https://github.com/thrillfall/nextcloud-gpodder/pull/176) reported by @n0vella

### Changed
- 🧑‍💻 Switch entiere project to TypeScript

### Fixed
- 💄 Missing icon on home when aren't any favorites
- 💄 Tweaks spacing in several spaces on Home and banners
- 💩 Leverage the available space between the episode title and the play button (but hacky way for now)
[#59](https://git.crystalyx.net/Xefir/repod/issues/59#issuecomment-6246) reported by @W_LL_M

## 3.1.0 - Above the stars - 2024-09-02

### Added
- ⭐ You can now add favorites subscriptions !
It will show's up on the homepage instead of the recommendations witch appear only when you add a new subscription.
[#59](https://git.crystalyx.net/Xefir/repod/issues/59) suggested by @W_LL_M, @Jaunty and @Satalink

### Changed
- 💥 Use html5 routing instead of hashes. All the URLs has changed removing the `#/` part.

### Fixed
- 🐛 Regression on 3.0 that prevent seeking player to episode last listened position
[#136](https://git.crystalyx.net/Xefir/repod/issues/136) reported by @randomuser1967
- ⚡ Improve the detection off mis-installed or mis-enabled gpodder app

## 3.0.0 - What a vue - 2024-08-17

### Added
- 🌐 Add german translation
Thanks to @OiledAmoeba [#120](https://git.crystalyx.net/Xefir/repod/issues/120)

### Changed
- 🎉 Migrate to Vue 3
- 🔖 Support Nextcloud 30
- 🏗️ Switch from Vuex to Pinia

### Fixed
- 💄 Use iTunes image first for episode if available
- 💄 Displaying styles and proper HTML on episode's modal descriptions

### Removed
- 🗑️ Temporary replacing @nextcloud/dialogs to toastyjs

## 2.3.3 - The Cake is a Lie - 2024-06-14

### Changed
- ⬆️ Update @nextcloud/dialogs to 5.3.2

### Fixed
- 🐛 App crashed when no cache system available
[#107](https://git.crystalyx.net/Xefir/repod/issues/107) reported by @skvaller and @PhilTraere

## 2.3.2 - Young Youth - 2024-05-31

### Fixed
- 🐛 New subscribe button on search not disapearing if subscribed
- ♿ Missing accessibility label on this button as well

## 2.3.1 - Powerwash the Universe - 2024-05-29

### Changed
- ⚡ Reduce app size by not shipping sourcemap

## 2.3.0 - Star Align - 2024-05-29

### Added
- ➕ Ability to subscribe to podcast from search list
[#105](https://git.crystalyx.net/Xefir/repod/issues/105) suggested by @crystalyz

### Changed
- 🔖 Full support for Nextcloud 29
- ⬆️ Update @nextcloud/vue to 8.12.0
- 📄 Use only AGPL licence
- ♻️ Refacto based on the new app template from Nextcloud devs

### Removed
- 💀 Drop support for Nextcloud 26
- ⚰️ Drop support for PHP 8.0
- 🌐 Removed babel

## 2.2.1 - Shami was here - 2024-05-18

### Removed
- ♻️ Rollback: Hide unreadable episodes because of insecure sources

## 2.2.0 - Moving in and out - 2024-05-18

### Added
- 🚨 Linting the code with ESLint
- 🎨 Prettierify the code

### Changed
- ⬆️ Update all dependancies

### Fixed
- 🔓 Hide unreadable episodes because of insecure sources

## 2.1.0 - Pocket Gundams - 2024-03-16

### Added
- 🔍 Add CTA for rating the app on the store

### Changed
- ⬆️ Update @nextcloud/dialogs to 5.2.0
- ⬆️ Update @nextcloud/vue to 8.11.0
- 🔖 Set compatibility with Nextcloud 29

### Fixed
- 🔒 App wasn't working for non admin users
[#76](https://git.crystalyx.net/Xefir/repod/issues/76) reported by @devasservice

## 2.0.0 - Taking Actions - 2024-03-05

### Added
- 🍪 Saving filters preference
[#66](https://git.crystalyx.net/Xefir/repod/issues/66) suggested by @jeef
- 📋 Add several options on each episode :
  - Mark as read
  - Open the webpage of the episode
  - Download the episode
- ↪️ Any actions will be reflected on the episode's list

### Changed
- ⬆️ Update @nextcloud/vue to 8.8.1

### Fixed
- ❤️‍🔥 Better handling ended episodes

## 1.5.9 - Just According to Keikaku - 2024-02-21

### Changed
- 🧑‍💻 Change some endpoints to match gPodder.net "specifications"
- ⬆️ Update @nextcloud/vue to 8.7.0

## 1.5.8 - Goblet of Eonothem - 2024-02-11

### Fixed
- Fyyd API sometime send empty feeds, ignoring them

## 1.5.7 - 2024-02-08

### Removed
- Proxy episodes when they are behind an unsecure http server

## 1.5.6 - 2024-02-07

### Added
- Proxy episodes when they are behind an unsecure http server

### Changed
- Update @nextcloud/vue to v8.6.2

## 1.5.5 - Hide and seek - 2024-02-04

### Fixed
- Can't open podcast details if cache missing or misconfigured
[#58](https://git.crystalyx.net/Xefir/repod/issues/58) reported by @raxventus

## 1.5.4 - In search of the truth - 2024-02-03

### Fixed
- Nextcloud search engine didn't work on Nextcloud 26 and 27
[#57](https://git.crystalyx.net/Xefir/repod/issues/57) reported by @JonOfUs

## 1.5.3 - The date where it all ends - 2024-02-01

### Changed
- Update @nextcloud/vue to v8.6.1

### Fixed
- Fix episode listing crashing if an invalid publication date is present in the RSS

## 1.5.2 - A little to the top - 2024-02-01

### Changed
- Update @nextcloud/router to v3.0.0

### Fixed
- Fix player alignment off by a couple of pixels

## 1.5.1 - Play on the PlayHead - 2024-01-30

### Changed
- Update @nextcloud/vue to v8.6.0
- Change the player progress bar to the native browser component
[#52](https://git.crystalyx.net/Xefir/repod/issues/52) suggested by @W_LL_M

### Fixed
- Force the placement of the filter settings to the top

## 1.5.0 - Featuring the filtering - 2024-01-30

### Added
- Filtering options for each podcast section
[#50](https://git.crystalyx.net/Xefir/repod/issues/50) suggested by @W_LL_M

### Changed
- Update @nextcloud/router to v2.2.1
- Update @nextcloud/dialogs to v5.1.1
- Update @nextcloud/vue to v8.5.1
- Update vue-material-deisgn-icons to v5.3.0
- Displaying episode publication as "dd mmm yyyy" instead of xyz ago
[#48](https://git.crystalyx.net/Xefir/repod/issues/48) suggested by @W_LL_M
- Better display the reading status for episodes
[#51](https://git.crystalyx.net/Xefir/repod/issues/51) suggested by @W_LL_M

### Fixed
- When exporting feeds, if the RSS server fails, the export continue

### Removed
- Remove @nextcloud/moment

## 1.4.4 - 2024-01-24

### Changed
- Update @nextcloud/vue to v8.5.0

## 1.4.3 - 2024-01-21

### Added
- Expose the feed URL
[#41](https://git.crystalyx.net/Xefir/repod/issues/41) suggested by @SteveDinn

### Fixed
- More granular playback speed adustment by steps of 0.1
[#40](https://git.crystalyx.net/Xefir/repod/issues/40) reported by @SteveDinn

## 1.4.2 - 2024-01-20

### Fixed
- Ended episodes still didn't show well, should be fixed now hopefully

## 1.4.1 - 2024-01-19

### Fixed
- When deleting an episode on AntennaPod or on GPodder, it shows ended as expected
- Add space in the bottom of tops to allow catching the scrollbar

## 1.4.0 - 2024-01-18

### Added
- Import subscriptions
- Export subscriptions

## 1.3.0 - 2024-01-18

### Added
- Unified search integration

## 1.2.1 - 2024-01-18

### Changed
- When click on the title of the podcast on the player bar, it now opens the description modal
- Add icon on the playback speed setting

### Fixed
- Better fix for the background color of player bar on light theme
[#38](https://git.crystalyx.net/Xefir/repod/issues/38)

## 1.2.0 - 2024-01-17

### Added
- Add a playback speed setting
[#39](https://git.crystalyx.net/Xefir/repod/issues/39) suggested by @joezimjs

### Fixed
- Duration wrongly displayed
- Fix background color of player bar on light theme
[#38](https://git.crystalyx.net/Xefir/repod/issues/38) reported by @joezimjs
- Fix case of episodes not showing ended

## 1.1.2 - 2024-01-16

### Changed
- Sort subscriptions by listened time

### Removed
- Custom handler for redirections based on atom:link

## 1.1.1 - 2024-01-16

### Added
- This changelog

### Fixed
- An error occured if a duplicate is found in the search results or episodes list
- Offline downloaded episode was shown as played and ended

## 1.1.0 - 2024-01-15

### Added
- Show episode filesize on download description button
- Horizontal scrollbars on hot and new podcasts

### Changed
- Use moment instead of date-fns to have localized publication dates

### Fixed
- Missing title on description modal
- Display a generic avatar if author is missing
- Handle feed's redirections
- Sort search results and episodes by publication date

## 1.0.0 - 2024-01-11

- Initial release
