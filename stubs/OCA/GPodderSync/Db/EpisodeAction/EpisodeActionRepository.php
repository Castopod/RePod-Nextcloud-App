<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Db\EpisodeAction;

use OCA\GPodderSync\Core\EpisodeAction\EpisodeAction;

class EpisodeActionRepository
{
	public function __construct(
		private EpisodeActionMapper $episodeActionMapper
	) {}

	/**
	 * @return EpisodeAction[]
	 */
	public function findAll(int $sinceEpoch, string $userId) {}

	/**
	 * @return ?EpisodeAction
	 */
	public function findByEpisodeUrl(string $episodeUrl, string $userId) {}

	/**
	 * @return ?EpisodeAction
	 */
	public function findByGuid(string $guid, string $userId) {}

	public function deleteEpisodeActionByEpisodeUrl(string $episodeUrl, string $userId): void {}
}
