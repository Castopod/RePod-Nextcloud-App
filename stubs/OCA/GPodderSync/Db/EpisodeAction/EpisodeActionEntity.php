<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Db\EpisodeAction;

use OCA\GPodderSync\Core\EpisodeAction\EpisodeAction;
use OCP\AppFramework\Db\Entity;

/**
 * @psalm-import-type EpisodeActionType from EpisodeAction
 *
 * @method string getPodcast()
 * @method void   setPodcast(string $podcast)
 * @method string getEpisode()
 * @method void   setEpisode(string $episode)
 * @method string getAction()
 * @method void   setAction(string $action)
 * @method int    getTimestampEpoch()
 * @method void   setTimestampEpoch(mixed $timestampEpoch)
 * @method int    getStarted()
 * @method void   setStarted(integer $started)
 * @method int    getPosition()
 * @method void   setPosition(integer $position)
 * @method int    getTotal()
 * @method void   setTotal(integer $total)
 * @method string getGuid()
 * @method void   setGuid(string $guid)
 * @method string getUserId()
 * @method void   setUserId(string $userId)
 */
class EpisodeActionEntity extends Entity implements \JsonSerializable
{
	public $id;

	/**
	 * @return EpisodeActionType[]
	 */
	public function jsonSerialize(): mixed {}
}
