<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Db\EpisodeAction;

use OCP\AppFramework\Db\QBMapper;
use OCP\DB\Exception;
use OCP\IDBConnection;

/**
 * @template-extends QBMapper<EpisodeActionEntity>
 */
class EpisodeActionMapper extends QBMapper
{
	protected $tableName;
	protected $entityClass;
	protected $db;

	public function __construct(IDBConnection $db) {}

	/**
	 * @return EpisodeActionEntity[]
	 *
	 * @throws Exception
	 */
	public function findAll(int $sinceTimestamp, string $userId) {}

	/**
	 * @return ?EpisodeActionEntity
	 */
	public function findByEpisodeUrl(string $episodeIdentifier, string $userId) {}

	/**
	 * @return ?EpisodeActionEntity
	 */
	public function findByGuid(string $guid, string $userId) {}
}
