<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Db\EpisodeAction;

use OCP\DB\Exception;

class EpisodeActionWriter
{
	public function __construct(
		private EpisodeActionMapper $episodeActionMapper
	) {}

	/**
	 * @return EpisodeActionEntity
	 *
	 * @throws Exception
	 */
	public function save(EpisodeActionEntity $episodeActionEntity) {}

	/**
	 * @return EpisodeActionEntity
	 *
	 * @throws Exception
	 */
	public function update(EpisodeActionEntity $episodeActionEntity) {}
}
