<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Db\SubscriptionChange;

use OCP\AppFramework\Db\Entity;

class SubscriptionChangeEntity extends Entity implements \JsonSerializable
{
	public $id;

	/**
	 * @return array<string,mixed>
	 */
	public function jsonSerialize(): mixed {}
}
