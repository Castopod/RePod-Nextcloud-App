<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Db\SubscriptionChange;

class SubscriptionChangeWriter
{
	public function __construct(
		private SubscriptionChangeMapper $subscriptionChangeMapper
	) {}

	public function purge(): void {}

	/**
	 * @return SubscriptionChangeEntity
	 */
	public function create(SubscriptionChangeEntity $subscriptionChangeEntity) {}

	/**
	 * @return SubscriptionChangeEntity
	 */
	public function update(SubscriptionChangeEntity $subscriptionChangeEntity) {}
}
