<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Db\SubscriptionChange;

use OCP\AppFramework\Db\QBMapper;
use OCP\IDBConnection;

/**
 * @template-extends QBMapper<SubscriptionChangeEntity>
 */
class SubscriptionChangeMapper extends QBMapper
{
	protected $tableName;
	protected $entityClass;
	protected $db;

	public function __construct(IDBConnection $db) {}

	/**
	 * @return SubscriptionChangeEntity[]
	 */
	public function findAll(string $userId) {}

	/**
	 * @return ?SubscriptionChangeEntity
	 */
	public function findByUrl(string $url, string $userId) {}

	public function remove(SubscriptionChangeEntity $subscriptionChangeEntity): void {}

	/**
	 * @return SubscriptionChangeEntity[]
	 */
	public function findAllSubscriptionState(bool $subscribed, \DateTime $sinceTimestamp, string $userId) {}
}
