<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Db\SubscriptionChange;

class SubscriptionChangeRepository
{
	public function __construct(
		private SubscriptionChangeMapper $subscriptionChangeMapper
	) {}

	/**
	 * @return SubscriptionChangeEntity[]
	 */
	public function findAll() {}

	/**
	 * @return ?SubscriptionChangeEntity
	 */
	public function findByUrl(string $episode, string $userId) {}

	/**
	 * @return SubscriptionChangeEntity[]
	 */
	public function findAllSubscribed(\DateTime $sinceTimestamp, string $userId) {}

	/**
	 * @return SubscriptionChangeEntity[]
	 */
	public function findAllUnSubscribed(\DateTime $sinceTimestamp, string $userId) {}
}
