<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Core\SubscriptionChange;

class SubscriptionChangeRequestParser
{
	public function __construct(
		private SubscriptionChangesReader $subscriptionChangeReader
	) {}

	/**
	 * @return SubscriptionChange[]
	 */
	public function createSubscriptionChangeList(array $urlsSubscribed, array $urlsUnsubscribed) {}
}
