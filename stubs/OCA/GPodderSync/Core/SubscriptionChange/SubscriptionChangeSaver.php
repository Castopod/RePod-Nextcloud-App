<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Core\SubscriptionChange;

class SubscriptionChangeSaver
{
	public function saveSubscriptionChanges(array $urlsSubscribed, array $urlsUnsubscribed, string $userId): void {}
}
