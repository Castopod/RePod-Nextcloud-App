<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Core\SubscriptionChange;

class SubscriptionChange
{
	public function __construct(
		private string $url,
		private bool $isSubscribed
	) {}

	/**
	 * @return bool
	 */
	public function isSubscribed() {}

	/**
	 * @return string
	 */
	public function getUrl() {}
}
