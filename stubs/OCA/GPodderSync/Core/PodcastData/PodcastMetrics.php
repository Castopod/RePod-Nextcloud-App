<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Core\PodcastData;

/**
 * @psalm-type PodcastMetricsType = array{
 *  url: string,
 *  listenedSeconds: int,
 *  actionCounts: PodcastActionCounts
 * }
 */
class PodcastMetrics implements \JsonSerializable
{
	public function __construct(
		private string $url,
		private int $listenedSeconds = 0,
		private ?PodcastActionCounts $actionCounts = null
	) {}

	/**
	 * @return string
	 */
	public function getUrl() {}

	/**
	 * @return PodcastActionCounts
	 */
	public function getActionCounts() {}

	/**
	 * @return int
	 */
	public function getListenedSeconds() {}

	public function addListenedSeconds(int $seconds): void {}

	/**
	 * @return PodcastMetricsType
	 */
	public function toArray() {}

	/**
	 * @return PodcastMetricsType
	 */
	public function jsonSerialize(): mixed {}
}
