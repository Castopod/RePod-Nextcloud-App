<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Core\PodcastData;

/**
 * @psalm-type PodcastActionCountsType = array{
 *  delete: int,
 *  download: int,
 *  flattr: int,
 *  new: int,
 *  play: int
 * }
 */
class PodcastActionCounts implements \JsonSerializable
{
	public function incrementAction(string $action): void {}

	/**
	 * @return PodcastActionCountsType
	 */
	public function toArray() {}

	/**
	 * @return PodcastActionCountsType
	 */
	public function jsonSerialize(): mixed {}
}
