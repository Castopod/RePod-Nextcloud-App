<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Core\PodcastData;

class PodcastDataReader
{
	/**
	 * @return ?PodcastData
	 */
	public function getCachedOrFetchPodcastData(string $url, string $userId) {}

	/**
	 * @return ?PodcastData
	 */
	public function fetchPodcastData(string $url, string $userId) {}

	/**
	 * @return ?PodcastData
	 */
	public function tryGetCachedPodcastData(string $url) {}

	/**
	 * @return bool
	 */
	public function trySetCachedPodcastData(string $url, PodcastData $data) {}
}
