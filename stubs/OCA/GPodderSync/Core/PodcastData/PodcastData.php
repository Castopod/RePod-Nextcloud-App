<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Core\PodcastData;

/**
 * @psalm-type PodcastDataType = array{
 *  title: ?string,
 *  author: ?string,
 *  link: ?string,
 *  description: ?string,
 *  imageUrl: ?string,
 *  fetchedAtUnix: int,
 *  imageBlob: ?string
 * }
 */
class PodcastData implements \JsonSerializable
{
	public function __construct(
		private ?string $title,
		private ?string $author,
		private ?string $link,
		private ?string $description,
		private ?string $imageUrl,
		private int $fetchedAtUnix,
		private ?string $imageBlob = null
	) {}

	/**
	 * @return PodcastData
	 *
	 * @throws \Exception if the XML data could not be parsed
	 */
	public static function parseRssXml(string $xmlString, ?int $fetchedAtUnix = null) {}

	/**
	 * @return ?string
	 */
	public function getTitle() {}

	/**
	 * @return ?string
	 */
	public function getAuthor() {}

	/**
	 * @return ?string
	 */
	public function getLink() {}

	/**
	 * @return ?string
	 */
	public function getDescription() {}

	/**
	 * @return ?string
	 */
	public function getImageUrl() {}

	/**
	 * @return ?int
	 */
	public function getFetchedAtUnix() {}

	/**
	 * @return ?string
	 */
	public function getImageBlob() {}

	public function setImageBlob(?string $blob): void {}

	/**
	 * @return PodcastDataType
	 */
	public function toArray() {}

	/**
	 * @return PodcastDataType
	 */
	public function jsonSerialize(): mixed {}

	/**
	 * @param PodcastDataType $data
	 *
	 * @return PodcastData
	 */
	public static function fromArray(array $data) {}
}
