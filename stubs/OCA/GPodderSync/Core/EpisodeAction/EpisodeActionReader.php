<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Core\EpisodeAction;

class EpisodeActionReader
{
	/**
	 * @param array $episodeActionsArray []
	 *
	 * @return EpisodeAction[]
	 *
	 * @throws \InvalidArgumentException
	 */
	public function fromArray(array $episodeActionsArray) {}
}
