<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Core\EpisodeAction;

/**
 * @psalm-type EpisodeActionType = array{
 *  podcast: string,
 *  episode: string,
 *  action: string,
 *  timestamp: string,
 *  started: int,
 *  position: int,
 *  total: int,
 *  guid: ?string,
 *  id: int
 * }
 */
class EpisodeAction
{
	public function __construct(
		private string $podcast,
		private string $episode,
		private string $action,
		private string $timestamp,
		private int $started,
		private int $position,
		private int $total,
		private ?string $guid,
		private ?int $id
	) {}

	/**
	 * @return string
	 */
	public function getPodcast() {}

	/**
	 * @return string
	 */
	public function getEpisode() {}

	/**
	 * @return string
	 */
	public function getAction() {}

	/**
	 * @return string
	 */
	public function getTimestamp() {}

	/**
	 * @return int
	 */
	public function getStarted() {}

	/**
	 * @return int
	 */
	public function getPosition() {}

	/**
	 * @return int
	 */
	public function getTotal() {}

	/**
	 * @return ?string
	 */
	public function getGuid() {}

	/**
	 * @return int
	 */
	public function getId() {}

	/**
	 * @return EpisodeActionType
	 */
	public function toArray() {}
}
