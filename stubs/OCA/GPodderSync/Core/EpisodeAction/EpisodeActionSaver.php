<?php

declare(strict_types=1);

namespace OCA\GPodderSync\Core\EpisodeAction;

use OCA\GPodderSync\Db\EpisodeAction\EpisodeActionRepository;
use OCA\GPodderSync\Db\EpisodeAction\EpisodeActionWriter;

class EpisodeActionSaver
{
	public function __construct(
		private EpisodeActionRepository $episodeActionRepository,
		private EpisodeActionWriter $episodeActionWriter,
		private EpisodeActionReader $episodeActionReader
	) {}

	/**
	 * @return array
	 */
	public function saveEpisodeActions(array $episodeActionsArray, string $userId) {}
}
