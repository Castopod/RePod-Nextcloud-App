module.exports = {
	extends: [
		'@nextcloud/eslint-config/vue3',
		'plugin:pinia/recommended',
		'plugin:prettier/recommended',
	],
	rules: {
		'jsdoc/require-jsdoc': 'off',
		'vue/first-attribute-linebreak': 'off',
		'sort-imports': 'error',
		'vue/attributes-order': ['error', { alphabetical: true }],
	},
}
