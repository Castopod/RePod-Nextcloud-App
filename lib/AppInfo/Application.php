<?php

declare(strict_types=1);

namespace OCA\RePod\AppInfo;

use OCA\RePod\Service\SearchProvider;
use OCP\App\AppPathNotFoundException;
use OCP\App\IAppManager;
use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCP\AppFramework\Services\IInitialState;

class Application extends App implements IBootstrap
{
	public const APP_ID = 'repod';

	private const GPODDERSYNC_ID = 'gpoddersync';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}

	public function boot(IBootContext $context): void {
		/** @psalm-suppress DeprecatedInterface */
		$appContainer = $context->getAppContainer();

		/** @var IAppManager $appManager */
		$appManager = $appContainer->get(IAppManager::class);

		/** @var IInitialState $initialState */
		$initialState = $appContainer->get(IInitialState::class);

		if (null === $appManager->getAppInfo(self::GPODDERSYNC_ID)) {
			$appManager->disableApp(self::GPODDERSYNC_ID);
		}

		$gpoddersync = $appManager->isEnabledForUser(self::GPODDERSYNC_ID);
		if (!$gpoddersync) {
			try {
				$appManager->enableApp(self::GPODDERSYNC_ID);
			} catch (AppPathNotFoundException) {
			}
		}

		$gpoddersync = $appManager->isEnabledForUser(self::GPODDERSYNC_ID);
		$initialState->provideInitialState('gpodder', $gpoddersync);
	}

	public function register(IRegistrationContext $context): void {
		$context->registerSearchProvider(SearchProvider::class);
	}
}
