<?php

declare(strict_types=1);

namespace OCA\RePod\Core\EpisodeAction;

use OCA\GPodderSync\Core\EpisodeAction\EpisodeActionReader as CoreEpisodeActionReader;
use OCA\GPodderSync\Db\EpisodeAction\EpisodeActionRepository;
use OCA\RePod\Service\UserService;

class EpisodeActionReader extends CoreEpisodeActionReader
{
	public function __construct(
		private readonly EpisodeActionRepository $episodeActionRepository,
		private readonly UserService $userService
	) {}

	/**
	 * Base: https://github.com/pbek/nextcloud-nextpod/blob/main/lib/Core/EpisodeAction/EpisodeActionExtraData.php#L119.
	 * Specs : https://github.com/Podcast-Standards-Project/PSP-1-Podcast-RSS-Specification/blob/main/README.md.
	 *
	 * @return EpisodeActionExtraData[]
	 *
	 * @throws \Exception if the XML data could not be parsed
	 */
	public function parseRssXml(string $xmlString, ?int $fetchedAtUnix = null): array {
		$episodes = [];
		$xml = new \SimpleXMLElement($xmlString);
		$channel = $xml->channel;
		$title = (string) $channel->title;

		// Find episode by url and add data for it
		/** @var \SimpleXMLElement $item */
		foreach ($channel->item as $item) {
			$url = (string) $item->enclosure['url'];
			$type = (string) $item->enclosure['type'];
			$size = (int) $item->enclosure['length'];
			$guid = (string) $item->guid;

			$iTunesItemChildren = $item->children('itunes', true);
			$iTunesChannelChildren = $channel->children('itunes', true);

			// Get episode action
			$action = $this->episodeActionRepository->findByGuid($guid, $this->userService->getUserUID());

			if ($action) {
				$url = $action->getEpisode();
			} else {
				$action = $this->episodeActionRepository->findByEpisodeUrl($url, $this->userService->getUserUID());
			}

			// Get episode name
			$name = (string) $item->title;

			// Get episode link
			$link = $this->stringOrNull($item->link);

			// Get episode image
			if (isset($iTunesItemChildren)) {
				$imageAttributes = $iTunesItemChildren->image->attributes();
				$image = $this->stringOrNull(isset($imageAttributes) ? (string) $imageAttributes->href : '');
			}

			if (!isset($image) && isset($iTunesChannelChildren)) {
				$imageAttributes = $iTunesChannelChildren->image->attributes();
				$image = $this->stringOrNull(isset($imageAttributes) ? (string) $imageAttributes->href : '');
			}

			if (!isset($image)) {
				$image = $this->stringOrNull($item->image->url);
			}

			if (!isset($image)) {
				$image = $this->stringOrNull($channel->image->url);
			}

			if (!isset($image)) {
				preg_match('/<itunes:image\s+href="([^"]+)"/', $xmlString, $matches);
				if (count($matches) > 1) {
					$image = $this->stringOrNull($matches[1]);
				}
			}

			// Get episode author
			if (isset($iTunesItemChildren)) {
				$author = $this->stringOrNull($iTunesItemChildren->author);
			} else {
				$author = $this->stringOrNull($item->author);
			}

			// Get episode description
			$itemContent = $item->children('content', true);
			if (isset($itemContent)) {
				$description = $this->stringOrNull($itemContent->encoded);
			} else {
				$description = $this->stringOrNull($item->description);
			}

			if (!isset($description) && isset($iTunesItemChildren)) {
				$description = $this->stringOrNull($iTunesItemChildren->summary);
			}

			// Get episode duration
			if (isset($iTunesItemChildren)) {
				$duration = $this->stringOrNull($iTunesItemChildren->duration);
			} else {
				$duration = $this->stringOrNull($item->duration);
			}

			// Get episode pubDate
			$pubDate = $this->stringOrNull($item->pubDate);
			if (isset($pubDate)) {
				try {
					$pubDate = new \DateTime($pubDate);
				} catch (\Exception) {
					$pubDate = null;
				}
			}

			$episodes[] = new EpisodeActionExtraData(
				$title,
				$url,
				$name,
				$link,
				$image,
				$author,
				$description,
				$fetchedAtUnix ?? (new \DateTime())->getTimestamp(),
				$guid,
				$type,
				$size,
				$pubDate,
				$duration,
				$action
			);
		}

		return $episodes;
	}

	/**
	 * @param null|\SimpleXMLElement|string $value
	 */
	private function stringOrNull($value): ?string {
		/** @psalm-suppress RiskyTruthyFalsyComparison */
		if (!empty($value)) {
			return (string) $value;
		}

		return null;
	}
}
