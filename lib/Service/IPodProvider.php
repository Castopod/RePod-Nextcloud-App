<?php

declare(strict_types=1);

namespace OCA\RePod\Service;

use OCA\GPodderSync\Core\PodcastData\PodcastData;

interface IPodProvider
{
	/**
	 * @return PodcastData[]
	 */
	public function search(string $value): array;
}
