<?php

declare(strict_types=1);

namespace OCA\RePod\Service;

use OCA\GPodderSync\Core\PodcastData\PodcastData;
use OCP\Http\Client\IClientService;

class ItunesService implements IPodProvider
{
	private const BASE_URL = 'https://itunes.apple.com/';

	public function __construct(
		private readonly IClientService $clientService,
		private readonly UserService $userService
	) {}

	public function search(string $value): array {
		$podcasts = [];

		$client = $this->clientService->newClient();
		$response = $client->get(self::BASE_URL.'search', [
			'query' => [
				'media' => 'podcast',
				'term' => $value,
				'country' => $this->userService->getCountryCode(),
			],
		]);
		$json = (array) json_decode((string) $response->getBody(), true, flags: JSON_THROW_ON_ERROR);

		if (array_key_exists('results', $json) && is_array($json['results'])) {
			/** @var string[] $feed */
			foreach ($json['results'] as $feed) {
				$time = strtotime($feed['releaseDate']);
				$podcasts[] = new PodcastData(
					$feed['trackName'],
					$feed['artistName'],
					$feed['feedUrl'],
					$feed['primaryGenreName'],
					$feed['artworkUrl600'],
					is_int($time) ? $time : time()
				);
			}
		}

		return $podcasts;
	}
}
