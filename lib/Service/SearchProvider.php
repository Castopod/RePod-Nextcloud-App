<?php

declare(strict_types=1);

namespace OCA\RePod\Service;

use OCA\RePod\AppInfo\Application;
use OCP\IL10N;
use OCP\IURLGenerator;
use OCP\IUser;
use OCP\Search\IProvider;
use OCP\Search\ISearchQuery;
use OCP\Search\SearchResult;
use OCP\Search\SearchResultEntry;

class SearchProvider implements IProvider
{
	public function __construct(
		private readonly IL10N $l10n,
		private readonly IURLGenerator $urlGenerator,
		private readonly MultiPodService $multiPodService
	) {}

	public function getId(): string {
		return Application::APP_ID;
	}

	public function getName(): string {
		return $this->l10n->t('Podcast');
	}

	public function getOrder(string $route, array $routeParameters): int {
		if (str_starts_with($route, Application::APP_ID.'.')) {
			// Active app, prefer my results
			return -1;
		}

		return 25;
	}

	public function search(IUser $user, ISearchQuery $query): SearchResult {
		$podcasts = $this->multiPodService->search($query->getTerm());

		$searchResults = [];
		foreach ($podcasts as $podcast) {
			$title = $podcast->getTitle();
			$link = $podcast->getLink();

			if (isset($title, $link)) {
				$searchResults[] = new SearchResultEntry(
					$podcast->getImageUrl() ?? $this->urlGenerator->linkTo(Application::APP_ID, 'img/app.svg'),
					$title,
					$podcast->getAuthor() ?? '',
					$this->urlGenerator->linkToRoute('repod.page.index').'/#/'.urlencode(base64_encode($link)),
					$this->urlGenerator->linkTo(Application::APP_ID, 'img/app.svg')
				);
			}
		}

		return SearchResult::complete(
			$this->l10n->t('Podcast'),
			$searchResults
		);
	}
}
