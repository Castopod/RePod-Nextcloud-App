<?php

declare(strict_types=1);

namespace OCA\RePod\Service;

use OCA\GPodderSync\Core\PodcastData\PodcastData;
use Psr\Log\LoggerInterface;

class MultiPodService implements IPodProvider
{
	/**
	 * @var IPodProvider[]
	 */
	private array $providers = [];

	public function __construct(
		FyydService $fyydService,
		ItunesService $itunesService,
		private readonly LoggerInterface $logger
	) {
		$this->providers = [$fyydService, $itunesService];
	}

	/**
	 * @return PodcastData[]
	 */
	public function search(string $value): array {
		$podcasts = [];

		foreach ($this->providers as $provider) {
			try {
				$podcasts = [...$podcasts, ...$provider->search($value)];
			} catch (\Exception $e) {
				$this->logger->error($e->getMessage(), $e->getTrace());
			}
		}

		usort($podcasts, fn (PodcastData $a, PodcastData $b): int => $b->getFetchedAtUnix() <=> $a->getFetchedAtUnix());

		return array_values(
			array_intersect_key(
				$podcasts,
				array_unique(
					array_map(
						fn (PodcastData $feed) => $feed->getLink(),
						array_filter($podcasts, fn (PodcastData $feed) => $feed->getLink())
					)
				)
			)
		);
	}
}
