<?php

declare(strict_types=1);

namespace OCA\RePod\Service;

use OCP\IUserSession;
use OCP\L10N\IFactory;

class UserService
{
	public function __construct(
		private readonly IFactory $l10n,
		private readonly IUserSession $userSession
	) {}

	public function getUserUID(): string {
		$user = $this->userSession->getUser();

		return $user ? $user->getUID() : '';
	}

	public function getIsoCode(): string {
		return $this->l10n->getUserLanguage($this->userSession->getUser());
	}

	public function getCountryCode(): string {
		$isoCodes = explode('_', $this->getIsoCode());

		return $isoCodes[1] ?? 'us';
	}

	public function getLangCode(): string {
		$isoCodes = explode('_', $this->getIsoCode());

		return $isoCodes[0];
	}
}
