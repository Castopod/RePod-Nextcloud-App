<?php

declare(strict_types=1);

namespace OCA\RePod\Controller;

use OCA\GPodderSync\Core\PodcastData\PodcastData;
use OCA\GPodderSync\Core\PodcastData\PodcastDataReader;
use OCA\RePod\AppInfo\Application;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\Attribute\FrontpageRoute;
use OCP\AppFramework\Http\Attribute\NoAdminRequired;
use OCP\AppFramework\Http\Attribute\NoCSRFRequired;
use OCP\AppFramework\Http\JSONResponse;
use OCP\Http\Client\IClientService;
use OCP\ICacheFactory;
use OCP\IRequest;

class PodcastController extends Controller
{
	public function __construct(
		IRequest $request,
		private readonly ICacheFactory $cacheFactory,
		private readonly IClientService $clientService,
		private readonly PodcastDataReader $podcastDataReader
	) {
		parent::__construct(Application::APP_ID, $request);
	}

	#[NoAdminRequired]
	#[NoCSRFRequired]
	#[FrontpageRoute(verb: 'GET', url: '/podcast')]
	public function index(string $url): JSONResponse {
		$podcast = null;

		if ($this->cacheFactory->isLocalCacheAvailable()) {
			try {
				$podcast = $this->podcastDataReader->tryGetCachedPodcastData($url);
			} catch (\Exception) {
			}
		}

		if ($podcast) {
			return new JSONResponse($podcast);
		}

		$client = $this->clientService->newClient();
		$feed = $client->get($url);
		$podcast = PodcastData::parseRssXml((string) $feed->getBody());

		if ($this->cacheFactory->isLocalCacheAvailable()) {
			try {
				$this->podcastDataReader->trySetCachedPodcastData($url, $podcast);
			} catch (\Exception) {
			}
		}

		/** @var Http::STATUS_* $returnStatusCode */
		$returnStatusCode = $feed->getStatusCode();

		return new JSONResponse($podcast, $returnStatusCode);
	}
}
