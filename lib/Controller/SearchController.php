<?php

declare(strict_types=1);

namespace OCA\RePod\Controller;

use OCA\RePod\AppInfo\Application;
use OCA\RePod\Service\MultiPodService;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\Attribute\FrontpageRoute;
use OCP\AppFramework\Http\Attribute\NoAdminRequired;
use OCP\AppFramework\Http\Attribute\NoCSRFRequired;
use OCP\AppFramework\Http\JSONResponse;
use OCP\IRequest;

class SearchController extends Controller
{
	public function __construct(
		IRequest $request,
		private readonly MultiPodService $multiPodService
	) {
		parent::__construct(Application::APP_ID, $request);
	}

	#[NoAdminRequired]
	#[NoCSRFRequired]
	#[FrontpageRoute(verb: 'GET', url: '/search')]
	public function index(string $q): JSONResponse {
		return new JSONResponse($this->multiPodService->search($q));
	}
}
