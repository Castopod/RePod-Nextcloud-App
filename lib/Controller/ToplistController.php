<?php

declare(strict_types=1);

namespace OCA\RePod\Controller;

use OCA\RePod\AppInfo\Application;
use OCA\RePod\Service\FyydService;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\Attribute\FrontpageRoute;
use OCP\AppFramework\Http\Attribute\NoAdminRequired;
use OCP\AppFramework\Http\Attribute\NoCSRFRequired;
use OCP\AppFramework\Http\JSONResponse;
use OCP\IRequest;

class ToplistController extends Controller
{
	public function __construct(
		IRequest $request,
		private readonly FyydService $fyydService
	) {
		parent::__construct(Application::APP_ID, $request);
	}

	#[NoAdminRequired]
	#[NoCSRFRequired]
	#[FrontpageRoute(verb: 'GET', url: '/toplist/hot')]
	public function hot(): JSONResponse {
		return new JSONResponse($this->fyydService->hot());
	}

	#[NoAdminRequired]
	#[NoCSRFRequired]
	#[FrontpageRoute(verb: 'GET', url: '/toplist/new')]
	public function new(): JSONResponse {
		return new JSONResponse($this->fyydService->latest());
	}
}
