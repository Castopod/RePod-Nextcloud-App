import { join, resolve } from 'path'
import { createAppConfig } from '@nextcloud/vite-config'
import { defineConfig } from 'vite'

const config = defineConfig({
	build: {
		sourcemap: false,
	},
})

export default createAppConfig(
	{
		main: resolve(join('src', 'main.ts')),
	},
	{
		config,
		createEmptyCSSEntryPoints: true,
		thirdPartyLicense: false,
	},
)
