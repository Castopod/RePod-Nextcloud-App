# https://github.com/nextcloud/appstore/blob/fixed-templates/nextcloudappstore/scaffolding/app-templates/26/app/Makefile

app_name=$(notdir $(CURDIR))
build_tools_directory=$(CURDIR)/build/tools
source_build_directory=$(CURDIR)/build/artifacts/source
source_package_name=$(source_build_directory)/$(app_name)
appstore_build_directory=$(CURDIR)/build/artifacts
appstore_package_name=$(appstore_build_directory)/$(app_name)
npm=$(shell which npm 2> /dev/null)
composer=$(shell which composer 2> /dev/null)

all: build

# Fetches the PHP and JS dependencies and compiles the JS. If no composer.json
# is present, the composer step is skipped, if no package.json or js/package.json
# is present, the npm step is skipped
.PHONY: build
build:
ifneq (,$(wildcard $(CURDIR)/composer.json))
	make composer
endif
ifneq (,$(wildcard $(CURDIR)/package.json))
	make npm
endif

# Installs and updates the composer dependencies. If composer is not installed
# a copy is fetched from the web
.PHONY: composer
composer:
ifeq (, $(composer))
	@echo "No composer command available, downloading a copy from the web"
	mkdir -p $(build_tools_directory)
	curl -sS https://getcomposer.org/installer | php
	mv composer.phar $(build_tools_directory)
	php $(build_tools_directory)/composer.phar install --prefer-dist
else
	composer install --prefer-dist
endif

# Installs npm dependencies
.PHONY: npm
npm:
	npm ci
	npm run build

# Removes the appstore build
.PHONY: clean
clean:
	rm -rf ./build

# Same as clean but also removes dependencies installed by composer, bower and
# npm
.PHONY: distclean
distclean: clean
	rm -rf vendor
	rm -rf node_modules
	rm -rf js/vendor
	rm -rf js/node_modules

# Builds the source and appstore package
.PHONY: dist
dist: build
	make source
	make appstore

# Builds the source package
.PHONY: source
source:
	rm -rf $(source_build_directory)
	mkdir -p $(source_build_directory)
	tar -C .. -cvzf $(source_package_name).tar.gz \
	--exclude-vcs \
	--exclude="$(app_name)/build" \
	--exclude="$(app_name)/js/node_modules" \
	--exclude="$(app_name)/node_modules" \
	--exclude="$(app_name)/*.log" \
	--exclude="$(app_name)/js/*.log" \
	$(app_name)

# Builds the source package for the app store, ignores php tests, js tests
# and build related folders that are unnecessary for an appstore release
.PHONY: appstore
appstore:
	rm -rf $(appstore_build_directory)
	mkdir -p $(appstore_build_directory)
	tar -C .. -cvzf $(appstore_package_name).tar.gz \
	--exclude="$(app_name)/build" \
	--exclude="$(app_name)/tests" \
	--exclude="$(app_name)/Makefile" \
	--exclude="$(app_name)/*.log" \
	--exclude="$(app_name)/phpunit*xml" \
	--exclude="$(app_name)/composer.*" \
	--exclude="$(app_name)/node_modules" \
	--exclude="$(app_name)/js/node_modules" \
	--exclude="$(app_name)/js/tests" \
	--exclude="$(app_name)/js/test" \
	--exclude="$(app_name)/js/*.log" \
	--exclude="$(app_name)/js/package.json" \
	--exclude="$(app_name)/js/bower.json" \
	--exclude="$(app_name)/js/karma.*" \
	--exclude="$(app_name)/js/protractor.*" \
	--exclude="$(app_name)/package.json" \
	--exclude="$(app_name)/bower.json" \
	--exclude="$(app_name)/karma.*" \
	--exclude="$(app_name)/protractor\.*" \
	--exclude="$(app_name)/.*" \
	--exclude="$(app_name)/js/.*" \
	--exclude="$(app_name)/tsconfig.json" \
	--exclude="$(app_name)/stylelint.config.cjs" \
	--exclude="$(app_name)/README.md" \
	--exclude="$(app_name)/package-lock.json" \
	--exclude="$(app_name)/LICENSE" \
	--exclude="$(app_name)/src" \
	--exclude="$(app_name)/stubs" \
	--exclude="$(app_name)/screens" \
	--exclude="$(app_name)/vendor" \
	--exclude="$(app_name)/translationfiles" \
	--exclude="$(app_name)/Dockerfile" \
	--exclude="$(app_name)/psalm.xml" \
	--exclude="$(app_name)/renovate.json" \
	--exclude="$(app_name)/vite.config.ts" \
	$(app_name)

# Start a nextcloud server on Docker to kickstart developement
.PHONY: dev
dev: build
	docker stop $(app_name) || true
	docker rm $(app_name) || true
	docker build -t $(app_name) .
	docker run -itd --rm --name $(app_name) -v $(CURDIR):/var/www/html/apps/$(app_name) -p 80:80 $(app_name)
	npm run watch || docker stop $(app_name)

# Generate translations
.PHONY: l10n
l10n:
	docker run --rm \
		-v $(CURDIR):/app \
		--entrypoint php \
		nextcloudci/translations \
		/translationtool.phar create-pot-files
	docker run --rm \
		-v $(CURDIR):/app \
		--entrypoint php \
		nextcloudci/translations \
		/translationtool.phar convert-po-files
