# RePod Nextcloud App

This Nextcloud app lets you visualize, manage and play your favorite podcasts.

You need to have [GPodderSync](https://apps.nextcloud.com/apps/gpoddersync) installed to use this app!

## Features

- 🔍 Browse and subscribe huge collection of podcasts
- 🔊 Listen to episodes directly in Nextcloud
- 📋 Keep track of subscribed shows and episodes
- 🌐 Sync them with GPodderSync compatible clients
- 📱 Mobile friendly interface
- 📡 Import and export your subscriptions

## Comparaison with similar apps for Nextcloud

| | [RePod](https://apps.nextcloud.com/apps/repod) | [NextPod](https://apps.nextcloud.com/apps/nextpod) | [Music](https://apps.nextcloud.com/apps/music) | [Podcast](https://apps.nextcloud.com/apps/podcast) |
| --- | --- | --- | --- | --- |
| Actively maintened | ✅ | ✅ | ✅ | ❌ |
| Play your local music files | ❌ | ❌ | ✅ | ❌ |
| Sync with [GPodder clients](#clients-supporting-sync-of-gpoddersync) | ✅ | ✅ | [⭕](https://github.com/owncloud/music/issues/975) | [⭕](https://git.project-insanity.org/onny/nextcloud-app-podcast/-/issues/103) |
| Add and manage subscriptions | ✅ | ❌ | ✅ | ✅ |
| Listen synced episodes by another clients | ✅ | ✅ | ❌ | ❌ |
| Fetch and listen new epidodes | ✅ | [⭕](https://github.com/pbek/nextcloud-nextpod/issues/5) | ✅ | ✅ |
| Keep track of listened episodes | ✅ | ✅ | [⭕](https://github.com/owncloud/music/issues/1148) | ✅ |
| Download epidodes | ✅ | ✅ | ❌ | ✅ |
| Sleep timer | ✅ | ❌ | [⭕](https://github.com/owncloud/music/issues/884#issuecomment-921582302) | ❌ |
| Advanced player controls | ✅ | ❌ | ✅ | ✅ |
| Import and export subscriptions | ✅ | ❌ | [⭕](https://github.com/owncloud/music/issues/904) | [⭕](https://git.project-insanity.org/onny/nextcloud-app-podcast/-/issues/185) |
| Search and discover new podcasts | ✅ | ❌ | ❌ | ✅ |
| Open episode website and RSS feed | ✅ | ✅ | ❌ | ✅ |
| Integrate with Nextcloud search engine | ✅ | ❌ | ❌ | ✅ |
| Integrate with [Nextcloud Notes](https://apps.nextcloud.com/apps/notes) | ❌ | ✅ | ❌ | ❌ |
| Mobile friendly interface | ✅ | ❌ | ✅ | ✅ |
| Support chapters | ✅ | ❌ | ❌ | ✅ |
| Available in multiple languages | [✅](https://translate.crystalyx.net/projects/repod/gitea/) (en/fr/de) | ❌ | [✅](https://github.com/owncloud/music/issues/671#issuecomment-782746463) | [✅](https://www.transifex.com/project-insanityorg/podcast-1/dashboard/) (en/de) |

> Click on ⭕ to open the ticket

## Screenshots

<img src="./screens/index.png" width="230" title="Homepage" />
<img src="./screens/discover.png" width="230" title="Discover" />
<img src="./screens/search.png" width="230" title="Search" />
<img src="./screens/episodes.png" width="230" title="Episode list" />
<img src="./screens/modal.png" width="230" title="Episode description" />

## Clients supporting sync of GPodderSync

| client | support status |
| :- | :- |
| [AntennaPod](https://antennapod.org) | Initial purpose for this project, as a synchronization endpoint for this client.<br> Support is available [as of version 2.5.1](https://github.com/AntennaPod/AntennaPod/pull/5243/). |
| [KDE Kasts](https://apps.kde.org/de/kasts/) | Supported since version 21.12 |
| [Podcast Merlin](https://github.com/yoyoooooooooo/Podcast-Merlin--Nextcloud-Gpodder-Client-For-Windows) | Full sync support podcast client for Windows |
| [Cardo](https://cardo-podcast.github.io/#/cardo) | Podcast client with sync support, for Windows, Mac and Linux |

## Installation

Either from the official Nextcloud [app store](https://apps.nextcloud.com/apps/repod) or by downloading the [latest release](https://git.crystalyx.net/Xefir/repod/releases/latest) and extracting it into your Nextcloud `apps/` directory.

## Known issues

- Conflict with Plasma Integration Firefox addon ([#164](https://git.crystalyx.net/Xefir/repod/issues/164))

## Translations

You can contribute to translate the app in your language and you don't need to have any development background to do so !

Please join the effort at our **[Weblate](https://translate.crystalyx.net/projects/repod/gitea/)** project.

Thank you so much if you decide to participate ❤️

## Credits

- [GPodder Sync](https://github.com/thrillfall/nextcloud-gpodder) for the database API
- [Nextcloud Podcast](https://git.project-insanity.org/onny/nextcloud-app-podcast) for the basic UI ideas
- [NextPod](https://github.com/pbek/nextcloud-nextpod) for thier custom feed extraction code
