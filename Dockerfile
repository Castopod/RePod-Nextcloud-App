FROM nextcloud:30

ARG APP_NAME=repod
ENV NEXTCLOUD_UPDATE=1
ENV NEXTCLOUD_ADMIN_USER=$APP_NAME
ENV NEXTCLOUD_ADMIN_PASSWORD=$APP_NAME
ENV NEXTCLOUD_INIT_HTACCESS=1
ENV SQLITE_DATABASE=$APP_NAME

RUN apt-get update && \
    apt-get install -y nodejs npm sqlite3 && \
    rm -f /usr/local/etc/php/conf.d/opcache-recommended.ini && \
    /entrypoint.sh true

USER www-data

COPY --chown=www-data:www-data . apps/$APP_NAME
RUN cd apps/$APP_NAME && make build && cd - && \
    php occ app:install gpoddersync && \
    php occ app:enable $APP_NAME && \
    php occ config:system:set debug --value=true

USER root
