import { createRouter, createWebHistory } from 'vue-router'
import Discover from './views/Discover.vue'
import Feed from './views/Feed.vue'
import Home from './views/Home.vue'
import { generateUrl } from '@nextcloud/router'

const router = createRouter({
	history: createWebHistory(generateUrl('apps/repod')),
	routes: [
		{
			path: '/',
			component: Home,
		},
		{
			path: '/discover',
			component: Discover,
		},
		{
			path: '/feed/:url',
			component: Feed,
		},
	],
})

export default router
