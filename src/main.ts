import '@formatjs/intl-segmenter/polyfill'
import App from './App.vue'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import router from './router.ts'

const Vue = createApp(App)
const pinia = createPinia()

Vue.use(pinia)
Vue.use(router)
Vue.mount('#content')
