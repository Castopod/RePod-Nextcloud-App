import { getCookie, setCookie } from '../utils/cookies.ts'
import type { FiltersInterface } from '../utils/types.ts'
import { defineStore } from 'pinia'

export const useSettings = defineStore('settings', {
	state: () => {
		try {
			const filters = JSON.parse(getCookie('repod.filters') || '{}') || {}

			return {
				filters: {
					listened:
						filters.listened === undefined ? true : filters.listened,
					listening:
						filters.listening === undefined ? true : filters.listening,
					unlistened:
						filters.unlistened === undefined ? true : filters.unlistened,
				},
			}
		} catch {
			return {
				filters: {
					listened: true,
					listening: true,
					unlistened: true,
				},
			}
		}
	},
	actions: {
		setFilters(filters: Partial<FiltersInterface>) {
			this.filters = { ...this.filters, ...filters }
			setCookie('repod.filters', JSON.stringify(this.filters), 365)
		},
	},
})
