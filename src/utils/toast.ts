import toastify from 'toastify-js'

export const showMessage = (text: string, backgroundColor: string) =>
	toastify({
		text,
		backgroundColor,
	}).showToast()

export const showError = (text: string) => showMessage(text, 'var(--color-error)')
export const showWarning = (text: string) =>
	showMessage(text, 'var(--color-warning)')
export const showInfo = (text: string) => showMessage(text, 'var(--color-primary)')
export const showSuccess = (text: string) =>
	showMessage(text, 'var(--color-success)')
