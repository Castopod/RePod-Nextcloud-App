// https://grafikart.fr/tutoriels/javascript-cookies-2079
/**
 * Récupère les données associées à un cookie
 * @param {string} name Nom du cookie à récupérer
 * @return {string|null}
 */
export const getCookie = (name: string): string | null => {
	const cookies = document.cookie.split('; ')
	const value = cookies
		.find((c: string) => c.startsWith(name + '='))
		?.split('=')[1]
	if (value === undefined) {
		return null
	}
	return decodeURIComponent(value)
}

/**
 * Créer ou modifie la valeur d'un cookie avec une durée spécifique
 * @param {string} name Nom du cookie
 * @param {string} value Value du cookie
 * @param {number} days Durée de vie du cookie (en jours)
 */
export const setCookie = (name: string, value: string, days: number) => {
	const date = new Date()
	date.setDate(date.getDate() + days)
	document.cookie = `${name}=${encodeURIComponent(value)}; expires=${date.toUTCString()}; SameSite=Strict;`
}
