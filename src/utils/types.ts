export interface EpisodeActionInterface {
	podcast: string
	episode: string
	action: string
	timestamp: string
	started: number
	position: number
	total: number
	guid?: string
	id?: number
}

export interface EpisodeInterface {
	title: string
	url: string
	name: string
	link?: string
	image?: string
	author?: string
	description?: string
	fetchedAtUnix: number
	guid: string
	type?: string
	size?: number
	pubDate?: {
		date: string
		timezone_type: number
		timezone: string
	}
	duration?: string
	action?: EpisodeActionInterface
	selected?: boolean
}

export interface FiltersInterface {
	listened: boolean
	listening: boolean
	unlistened: boolean
}

export interface PodcastDataInterface {
	title: string
	author?: string
	link: string
	description?: string
	imageUrl?: string
	fetchedAtUnix: number
	imageBlob?: string | null
}

export interface PodcastMetricsInterface {
	url: string
	listenedSeconds: number
	actionCounts: {
		delete: number
		download: number
		flattr: number
		new: number
		play: number
	}
}

export interface SubscriptionInterface {
	data?: PodcastDataInterface
	isFavorite?: boolean
	metrics: PodcastMetricsInterface
}

export interface PersonalSettingsMetricsInterface {
	subscriptions: PodcastMetricsInterface[]
}

export interface PersonalSettingsPodcastDataInterface {
	data: PodcastDataInterface
}
